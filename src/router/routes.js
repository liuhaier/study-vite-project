const routes = [
  {
      path: '/',
      name: 'index',
      title: '首页',
      component: () => import('@/view/index/index.vue'), //.vue不能省略
  },
  {
      path: '/about',
      name: 'about',
      title: '关于我',
      component: () => import('@/view/about/index.vue'), //.vue不能省略
  },
]
export default routes
